import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController } from 'ionic-angular';

import { ProvidersCognitoProvider, CognitoResponse, CognitoResponseStatus } from '../../providers/providers-cognito/providers-cognito';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  account: { username: string, password: string } = { username: '', password: '' };

  constructor(public navCtrl: NavController,
    private alertCtrl: AlertController,
    private cognitoProvider: ProvidersCognitoProvider) {

  }

  doLogin() {
    const username = this.account.username;
    const password = this.account.password;
    let response: CognitoResponse;

    this.cognitoProvider.signIn(username, password)
      .then(result => {
        response = result;
        if (response.status === CognitoResponseStatus.LoggedIn) {
          // 認証成功
          return Promise.resolve(null);
        } else {
          // パスワード変更要求が返されたのでユーザに入力を求める
          return this.requestNewPassword();
        }
      })
      .then(newPassword => {
        if (response.status === CognitoResponseStatus.LoggedIn) {
          // 認証済み
          return Promise.resolve(response);
        }
        return this.cognitoProvider.changePw(response.cognitoUser, username, newPassword);
      })
      .then(response => {
        // 認証成功（ページ遷移）
        this.navCtrl.setRoot('MenuPage');
      })
      .catch(() => {
        alert('Login failed.');
      });
  }

  requestNewPassword(): Promise<string> {
    return new Promise<string>((resolve: Function, reject: Function) => {
      let newPassword: string = '';
      let prompt = this.alertCtrl.create({
        title: 'Login',
        message: 'Enter a new password',
        inputs: [
          {
            name: 'password',
            type: 'password',
            placeholder: 'Password'
          },
        ],
        buttons: [
          {
            text: 'Save',
            handler: data => {
              newPassword = data.password;
            }
          }
        ],
        enableBackdropDismiss: false
      });
  
      prompt.willUnload.subscribe(() => {
        console.log('willUnload');
        if (newPassword === '') {
          reject();
        } else {
          resolve(newPassword);
        }
      });
  
      prompt.present();
    });
  }


}
