import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ProvidersCognitoProvider } from '../../providers/providers-cognito/providers-cognito';
import { WebapiProvider, UserAcount } from '../../providers/webapi/webapi';

/**
 * Generated class for the UserRegistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-regist',
  templateUrl: 'user-regist.html',
})
export class UserRegistPage {

  account: UserAcount = {
    username: '',
    password: '',
    email: '',
    role: 'sales'
  };
  
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private cognitoProvider: ProvidersCognitoProvider,
    private webapi: WebapiProvider
  ) {
  }

  ionViewCanEnter(): Promise<void> {
    return this.cognitoProvider.validateSession();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserRegistPage');
  }

  doRegist() {
    this.webapi.createUser(this.account).then((response) => {
      alert('success!');
    })
    .catch((err) => {
      alert(err);
    });    
  }

}
