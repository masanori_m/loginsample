import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ProvidersCognitoProvider } from '../../providers/providers-cognito/providers-cognito';
import { WebapiProvider } from '../../providers/webapi/webapi';

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private cognitoProvider: ProvidersCognitoProvider,
    private webapi: WebapiProvider
  ) {
  }

  ionViewCanEnter(): Promise<void> {
    return this.cognitoProvider.validateSession();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPage');
  }

  doLogout() {
    this.cognitoProvider.signOut().then(() => {
      this.moveToHome();
    })
    .catch((err) => {
      console.log('Logout failed. ' + err);
      this.moveToHome();
    });
  }

  moveToHome() {
    this.navCtrl.setRoot('HomePage');
  }

  doRequest() {
    this.webapi.getDummy().then((response) => {
      alert(response);
    })
    .catch((err) => {
      alert(err);
    })
  }

  doGetUser() {
    this.webapi.getUser().then((response) => {
      alert(response);
    })
    .catch((err) => {
      alert(err);
    })    
  }

  popUserRegist() {
    this.navCtrl.push('UserRegistPage');
  }
}
