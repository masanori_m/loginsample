import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

import { environment } from '../../environments/environment';
import { ProvidersCognitoProvider } from '../../providers/providers-cognito/providers-cognito';

export interface UserAcount {
  username: string,
  password: string,
  email: string,
  role: string
}

/*
  Generated class for the WebapiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class WebapiProvider {

  constructor(public http: Http,
    private cognitoProvider: ProvidersCognitoProvider
  ) {
  }

  getDummy(): Promise<string> {
    return new Promise<string>((resolve: Function, reject: Function) => {
      this.cognitoProvider.getToken().then((tokenList) => {
        const headers = new Headers({ 'Authorization': tokenList[0] });
        const options = new RequestOptions({ headers: headers });
        this.http.get(environment.dummyApiURL, options)
                 .map(res => res.text())
                 .subscribe(
                   (text) => resolve(text),
                   (err) => reject(err));
      })
      .catch((err) => {
        reject(err);
      });
    });
  }

  getUser(): Promise<string> {
    return new Promise<string>((resolve: Function, reject: Function) => {
      this.cognitoProvider.getToken().then((tokenList) => {
        const headers = new Headers({ 'Authorization': tokenList[1] });
        const options = new RequestOptions({ headers: headers });
        this.http.get(environment.UserApiURL, options)
                 .map(res => res.text())
                 .subscribe(
                   (text) => resolve(text),
                   (err) => reject(err));
      })
      .catch((err) => {
        reject(err);
      });
    });
  }

  createUser(account: UserAcount): Promise<string> {
    return new Promise<string>((resolve: Function, reject: Function) => {
      this.cognitoProvider.getToken().then((tokenList) => {
        const headers = new Headers({ 'Authorization': tokenList[1] });
        const options = new RequestOptions({ headers: headers });
        this.http.post(environment.UserApiURL, account, options)
                 .map(res => res.text())
                 .subscribe(
                   (text) => resolve(text),
                   (err) => reject(err));
      })
      .catch((err) => {
        reject(err);
      });
    });
  }
}
