import { Injectable } from '@angular/core';

import * as AWS from "aws-sdk";
import { CognitoUserPool, CognitoUserSession, CognitoUser, AuthenticationDetails } from 'amazon-cognito-identity-js';
import { environment } from '../../environments/environment';


export enum CognitoResponseStatus {
  LoggedIn,
  NewPasswordRequired
}

export interface CognitoResponse {
  status: CognitoResponseStatus;
  cognitoUser?: CognitoUser;
}


/*
  Generated class for the ProvidersCognitoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProvidersCognitoProvider {
  userPool: CognitoUserPool = null;

  constructor() {
    AWS.config.region = environment.region;
    const data = {
      UserPoolId: environment.userPoolId,
      ClientId: environment.clientId,
      Storage: sessionStorage
    };
    this.userPool = new CognitoUserPool(data);
  }

  private createCognitoUser(username: string): CognitoUser {
    const userData = {
      Username : username,
      Pool : this.userPool,
      Storage: sessionStorage
    };
    return new CognitoUser(userData);
  }

  private getCognitoUser(): Promise<CognitoUser> {
    return new Promise<CognitoUser>((resolve: Function, reject: Function) => {
      const cognitoUser = this.userPool.getCurrentUser();
      if (!cognitoUser) {
        reject();
        return;
      }
      
      // UserPoolからcurrent userを取得しただけだと、セッションが復元されてないのでgetSessionを実行する
      cognitoUser.getSession((err, session) => {
        if (err || !session) {
          reject(err);
          return;
        }

        // 有効なセッションが設定されたCognitoUser
        resolve(cognitoUser);
      });
    });
  }

  signIn(username: string, password: string): Promise<CognitoResponse> {
    return new Promise<CognitoResponse>((resolve: Function, reject: Function) => {
      const cognitoUser = this.createCognitoUser(username);
      const authenticationData = {
          Username : username,
          Password : password
      };
      const authenticationDetails = new AuthenticationDetails(authenticationData);

      // 認証リクエストを送信
      cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: (session: CognitoUserSession, userConfirmationNecessary?: boolean) => {
          resolve({
            status: CognitoResponseStatus.LoggedIn
          });
        },
        onFailure: (err: any) => {
          reject(err);
        },
        newPasswordRequired: (userAttributes: any, requiredAttributes: any) => {
          // 初回ログイン時のパスワード変更要求なのでCognitoUserSessionは取得できていない
          resolve({
            status: CognitoResponseStatus.NewPasswordRequired,
            cognitoUser: cognitoUser
          });
        }
      });
    });
  }

  changePw(cognitoUser: CognitoUser, username: string, newPassword: string): Promise<CognitoResponse> {
    return new Promise<CognitoResponse>((resolve: Function, reject: Function) => {
      // 新パスワードを送信
      cognitoUser.completeNewPasswordChallenge(newPassword, null, {
        onSuccess: (session: CognitoUserSession) => {
          resolve({
            status: CognitoResponseStatus.LoggedIn
          });
        },
        onFailure: (err: any) => {
          reject(err);
        }
      });
    });    
  }

  validateSession(): Promise<void> {
    return new Promise<void>((resolve: Function, reject: Function) => {
      this.getCognitoUser().then((cognitoUser) => {
        resolve();
      })
      .catch((err) => {
        reject(err);
      });
    });      
  }

  signOut(): Promise<void> {
    return new Promise<void>((resolve: Function, reject: Function) => {
      this.getCognitoUser().then((cognitoUser) => {
        cognitoUser.globalSignOut({
          onSuccess: (msg: string) => {
            resolve();
          },
          onFailure: (err: Error) => {
            reject(err);
          }
        });
      })
      .catch((err) => {
        reject(err);
      });
    });      
  }

  getToken(): Promise<[string, string]> {
    return new Promise<[string, string]>((resolve: Function, reject: Function) => {
      this.getCognitoUser().then((cognitoUser) => {
        const session = cognitoUser.getSignInUserSession();
        if (!session) {
          reject();
          return;
        }

        resolve([session.getIdToken().getJwtToken(), session.getAccessToken().getJwtToken()]);
      })
      .catch((err) => {
        reject(err);
      });
    });    
  }
}
